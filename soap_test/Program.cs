﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using System.IO;
using System.Net;
using System.Net.Sockets;
using MySql.Data.MySqlClient;

namespace soap_test
{
    class Program
    {
        static void Main(string[] args)
        {
            //clean("Database=granit_loading;Data Source=localhost;User Id=root;Password=root123", "http://localhost:8888/");
            get_gu45_eids("Database=granit_loading;Data Source=localhost;User Id=root;Password=root123", "http://localhost:8888", 30000);
        }
        static void clean(string Connect, string address)
        {
            //delete old gu45
            int days_to_live = 2;
            DateTime dt = DateTime.Now;
            dt = dt.AddDays(-days_to_live);
            string date_str = dt.ToString("yyyy-MM-dd HH:mm:ss");

            MySqlConnection myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            string CommandText = "DELETE FROM `gu45` WHERE `created_time`<'" + date_str + "'";
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            myCommand.ExecuteNonQuery();
            myConnection.Close();
            //delete old trains
            int Months_to_live = 36;
            DateTime dt2 = DateTime.Now;
            dt2 = dt2.AddMonths(-Months_to_live);
            string date_str2 = dt2.ToString("yyyy-MM-dd HH:mm:ss");

            myConnection = new MySqlConnection(Connect);
            myConnection.Open();
            CommandText = "DELETE FROM `trains` WHERE `created_date`<'" + date_str2 + "'";
            myCommand = new MySqlCommand(CommandText, myConnection);
            myCommand.ExecuteNonQuery();
            myConnection.Close();

            System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\soap_log.txt", true);
            file.WriteLine(DateTime.Now +" hello");
            file.Close();
        }
        static void get_gu45_eids(string Connect, string address, int _timeout)
        {
            string CommandText = "select val from params where property='last_soap_query'";
            MySqlConnection myConnection = new MySqlConnection(Connect);
            MySqlCommand myCommand = new MySqlCommand(CommandText, myConnection);
            myConnection.Open();
            //get last gu45 date
            string date = myCommand.ExecuteScalar().ToString();
            myConnection.Close();
            DateTime dt = DateTime.Parse(date);
            DateTime shift_dt = dt.AddHours(-3); //
            string date_str = shift_dt.ToString("dd.MM.yyyy HH:mm:ss");
            //send soap

            string xml = requestGu45Eids(address, date_str, _timeout);

                //remove chunked
                int last_rn_index = -1;
                int second_rn=0;
                int first_rn=0;
                while (true)
                {
                    if ( (first_rn = xml.IndexOf("\r\n", last_rn_index+1)) !=-1)
                    {
                        last_rn_index = first_rn;
                        if ((second_rn = xml.IndexOf("\r\n", last_rn_index + 1)) != -1)
                        {
                            last_rn_index = second_rn;
                            xml=xml.Remove(first_rn, (second_rn - first_rn)+2);
                            Console.WriteLine("removed " + first_rn + " - " + (second_rn - first_rn));
                            last_rn_index = first_rn;
                        }
                        else break;
                    }
                    else break;
                }
                
                XmlDocument soapEnvelop = new XmlDocument();
                soapEnvelop.LoadXml(xml);
                //get eids and dates
                int[] EIDs = new int[100000];
                string[] dates = new string[100000];
                int i = 1;

                

                foreach (XmlNode n in soapEnvelop.GetElementsByTagName("ns1:rows").Item(0).ChildNodes)
                {
                    EIDs[i - 1] = Convert.ToInt32(n.FirstChild.FirstChild.InnerText);
                    int j = 0;
                    foreach (XmlNode n2 in n.FirstChild.ChildNodes)
                    {
                        j++;
                        if (j == 3) dates[i - 1] = n2.InnerText;
                    }
                    i++;
                }

            Console.WriteLine(String.Join(",", EIDs));
            Console.WriteLine(String.Join(",", dates));


            return;
            if (EIDs[0] > 0)
                {
                    i = 0;
                    int last_eid = 0;
                    myConnection = new MySqlConnection(Connect);
                    myConnection.Open();
                    CommandText = "";
                    myCommand = new MySqlCommand(CommandText, myConnection);
                    DateTime new_date = dt;
                    while (EIDs[i] > 0)
                    {
                        if (last_eid != EIDs[i])
                        {
                            //add eid 
                            myCommand.CommandText = "select eid from gu45 where eid=" + EIDs[i];
                            if (myCommand.ExecuteScalar() == null)
                            {
                                dt = DateTime.Parse(dates[i]);
                                date_str = dt.ToString("yyyy-MM-dd HH:mm:ss");
                                myCommand.CommandText = "INSERT INTO `gu45`(`eid`,`date`) VALUES (" + EIDs[i] + ",'" + date_str + "')";
                                myCommand.ExecuteNonQuery();
                                if (new_date < dt) new_date = dt;
                            }
                        }
                        last_eid = EIDs[i];
                        i++;
                    }
                    //set new date
                    myCommand.CommandText = "UPDATE `params` SET `val`='" + new_date.ToString("yyyy-MM-dd HH:mm:ss") + "' WHERE property='last_soap_query'";
                    myCommand.ExecuteNonQuery();
                    myConnection.Close();
                }
        }
        static String requestGu45Eids(String adress, String fromDate, int timeout)
        {
            var client = new RestSharp.RestClient(adress + "/proxy_dl.php");
            client.Timeout = timeout;
            var request = new RestSharp.RestRequest(RestSharp.Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("pragma", "no-cache");
            request.AddHeader("accept", "application/soap+xml, application/dime, multipart/related, text/*");
            request.AddHeader("soapaction", "\"\"");
            request.AddHeader("charset", "utf-8");
            request.AddHeader("content-type", "text/xml");
            request.AddParameter("text/xml", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\r\n  <soapenv:Body>\r\n    <getDocuments xmlns=\"http://ep.isc.by/ep-ws/services/EPServer3\">\r\n      <nmDocument>GU45_EXT_1</nmDocument>\r\n      <table>\r\n        <ns1:columnNames xmlns:ns1=\"http://typeexchange.isc.rw\">\r\n          <item>DAT</item>\r\n          <item>STATIONC</item>\r\n        </ns1:columnNames>\r\n        <ns2:columnTypes xmlns:ns2=\"http://typeexchange.isc.rw\">\r\n          <item>T</item>\r\n          <item>C</item>\r\n        </ns2:columnTypes>\r\n        <ns3:rows xmlns:ns3=\"http://typeexchange.isc.rw\">\r\n          <item>\r\n            <ns3:data>\r\n              <item>"+ fromDate +"</item>\r\n              <item>152006</item>\r\n            </ns3:data>\r\n          </item>\r\n        </ns3:rows>\r\n        <ns4:tableName xsi:nil=\"true\" xmlns:ns4=\"http://typeexchange.isc.rw\"/>\r\n      </table>\r\n      <id>6c9352af-e40b-48d8-9839-5e86bd58f6e7</id>\r\n    </getDocuments>\r\n  </soapenv:Body>\r\n</soapenv:Envelope>", RestSharp.ParameterType.RequestBody);
            RestSharp.IRestResponse response = client.Execute(request);
            return response.Content;
        }
    }
}



